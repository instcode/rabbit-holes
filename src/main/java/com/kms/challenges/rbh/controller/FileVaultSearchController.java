package com.kms.challenges.rbh.controller;

import com.kms.challenges.rbh.domain.FileMetadata;
import com.kms.challenges.rbh.repository.FileVaultRepository;
import com.kms.challenges.rbh.service.vault.FileVaultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.List;

/**
 * @author tkhuu.
 */
@Controller
public class FileVaultSearchController {
    private static final String VIEW_SEARCH_RESULT = "search";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileVaultSearchController.class.getCanonicalName());

    @Autowired
    private FileVaultRepository fileVaultRepository;

    @Autowired
    private FileVaultService fileVaultService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String list(@CookieValue(value = "admin", required = false, defaultValue = "false") Boolean admin,
                       @RequestParam(value = "searchText",required = false) String searchText, Model
            model) throws SQLException {
        List<FileMetadata> fileMetadataList;
        if (searchText != null) {
            fileMetadataList = fileVaultService.search(searchText);
        } else {
            fileMetadataList = fileVaultRepository.findAll();
        }
        model.addAttribute("isAdmin", admin);
        model.addAttribute("fileMetadataList", fileMetadataList);
        return VIEW_SEARCH_RESULT;
    }
}
