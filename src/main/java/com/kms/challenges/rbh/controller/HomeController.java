package com.kms.challenges.rbh.controller;

import com.kms.challenges.rbh.domain.FileMetadata;
import com.kms.challenges.rbh.model.SimpleUserDetails;
import com.kms.challenges.rbh.repository.FileVaultRepository;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    private static final String REDIRECT_FILEVAULT = "index";

    @Autowired
    private FileVaultRepository fileVaultRepository;

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String showHomePage(Model model, HttpServletResponse response) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        if (principal instanceof SimpleUserDetails) {
            String userName = ((SimpleUserDetails) principal).getUsername();
            isAdmin = userName.equals("admin@localhost");
            response.addCookie(new Cookie("admin", Boolean.toString(isAdmin)));
        }
        List<FileMetadata> fileMetadataList = fileVaultRepository.findAll();

        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("fileMetadataList", fileMetadataList);
        LOGGER.debug("Rendering homepage.");
        return REDIRECT_FILEVAULT;
    }
}
