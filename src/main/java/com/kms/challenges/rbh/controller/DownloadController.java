package com.kms.challenges.rbh.controller;

import com.kms.challenges.rbh.model.SimpleUserDetails;
import org.h2.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author instcode.
 */
@Controller
public class DownloadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadController.class.getCanonicalName());

    public static final String FILE_STORAGE_LOCATION = "./files/";

    @RequestMapping(value = "/download/{userid}", method = RequestMethod.GET)
    public void retrieve(@RequestParam("name") String fileName, @PathVariable("userid") String userId,
                         HttpServletResponse response) {
        String filePath = FILE_STORAGE_LOCATION + "/" + userId + "/" + fileName;
        File file = new File(filePath);
        try {
            InputStream is = new FileInputStream(file);
            IOUtils.copy(is, response.getOutputStream());
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.flushBuffer();
            is.close();
        } catch (IOException ex) {
            LOGGER.info("Error writing file to output stream", ex);
            throw new RuntimeException("Error when writing file from " + file.getAbsoluteFile());
        }
    }
}
