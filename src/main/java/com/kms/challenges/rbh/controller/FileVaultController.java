package com.kms.challenges.rbh.controller;

import com.kms.challenges.rbh.domain.FileMetadata;
import com.kms.challenges.rbh.domain.User;
import com.kms.challenges.rbh.model.SimpleUserDetails;
import com.kms.challenges.rbh.repository.FileVaultRepository;
import com.kms.challenges.rbh.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.List;

/**
 * @author tkhuu.
 */
@Controller
public class FileVaultController {
    private static final String VIEW_GUESTBOOK = "filevault";
    private static final String VIEW_UPLOAD = "upload";
    private static final String REDIRECT_UPLOAD = "redirect:/upload";
    private static final String REDIRECT_HOME = "redirect:/";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileVaultController.class.getCanonicalName());

    @Autowired
    private FileVaultRepository fileVaultRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public String upload(Model model) {
        String email = ((SimpleUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepository.findByEmail(email);
        List<FileMetadata> fileMetadataList = fileVaultRepository.findByUser(user);
        model.addAttribute("fileMetadataList", fileMetadataList);
        return VIEW_UPLOAD;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("desc") String desc) {
        Long userId = ((SimpleUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getId();
        String fileLocation = DownloadController.FILE_STORAGE_LOCATION + "/" + userId + "/" + file
                .getOriginalFilename();
        File parrentFolder = new File(DownloadController.FILE_STORAGE_LOCATION + "/" + userId);
        File saveFile = new File(fileLocation);
        BufferedOutputStream outputStream = null;
        try {
            parrentFolder.mkdirs();
            saveFile.createNewFile();
            outputStream = new BufferedOutputStream(
                    new FileOutputStream(saveFile));
            outputStream.write(file.getBytes());
        } catch (IOException e) {
            LOGGER.error("Can't save file", e);
        }
        FileMetadata fileMetadata = new FileMetadata();
        fileMetadata.setUser(userRepository.findOne(userId));
        fileMetadata.setFileName(saveFile.getName());
        fileMetadata.setDescription(desc);
        fileVaultRepository.saveAndFlush(fileMetadata);
        return REDIRECT_UPLOAD;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteFile(HttpServletRequest request, @CookieValue(value = "admin", required = false, defaultValue = "false") Boolean admin,
                             @RequestParam("fileId") Long id, @RequestParam("r") String redirect) {
        Long currentUserId = ((SimpleUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getId();
        FileMetadata metadata = fileVaultRepository.findOne(id);
        if (metadata.getUser().getId().equals(currentUserId) || admin) {
            fileVaultRepository.delete(id);
            fileVaultRepository.flush();
            File file = new File(
                    DownloadController.FILE_STORAGE_LOCATION + "/" + metadata.getUser().getId() + "/" + metadata
                            .getFileName());
            file.delete();
        } else {
            throw new RuntimeException("You don't have permission to delete this file");
        }
        if (redirect.equals("home")) return REDIRECT_HOME;
        return REDIRECT_UPLOAD;
    }
}
