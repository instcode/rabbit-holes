package com.kms.challenges.rbh.repository;

import com.kms.challenges.rbh.domain.FileMetadata;
import com.kms.challenges.rbh.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author tkhuu.
 */
public interface FileVaultRepository extends JpaRepository<FileMetadata,Long> {
    List<FileMetadata> findByUser(User user);
}
