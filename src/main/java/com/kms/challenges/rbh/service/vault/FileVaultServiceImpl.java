package com.kms.challenges.rbh.service.vault;

import com.kms.challenges.rbh.domain.FileMetadata;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileVaultServiceImpl implements FileVaultService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<FileMetadata> search(String searchText) {
        List<FileMetadata> fileMetadataList = new ArrayList<FileMetadata>();

        String sql = "select * from file_metadata where " +
                "file_name LIKE '%" + searchText + "%' or " +
                "description LIKE '%" + searchText + "%'";
        Query nativeQuery = entityManager.createNativeQuery(sql, FileMetadata.class);
        List resultList = nativeQuery.getResultList();
        for (Object resultSet : resultList) {
            FileMetadata metadata = (FileMetadata) resultSet;
            fileMetadataList.add(metadata);
        }
        return fileMetadataList;
    }
}