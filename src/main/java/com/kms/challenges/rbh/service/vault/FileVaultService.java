package com.kms.challenges.rbh.service.vault;

import com.kms.challenges.rbh.domain.FileMetadata;
import com.kms.challenges.rbh.domain.User;
import com.kms.challenges.rbh.model.RegistrationForm;

import java.util.List;

public interface FileVaultService {

    List<FileMetadata> search(String searchText);
}
