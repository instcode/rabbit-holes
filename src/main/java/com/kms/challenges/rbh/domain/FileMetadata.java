package com.kms.challenges.rbh.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * @author tkhuu.
 */
@Entity
@Table(name = "file_metadata")
public class FileMetadata extends BaseEntity<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NotEmpty
    @Column(length = 500,name = "file_name")
    private String fileName;
    @Column(length = 500,name = "description")
    private String description;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
