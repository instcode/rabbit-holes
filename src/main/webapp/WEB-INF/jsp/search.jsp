<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:layout>
    <jsp:attribute name="header"><spring:message code="rabbitholes.homepage.title"/></jsp:attribute>
    <jsp:body>
        <sec:authentication property="principal.id" var="user_id"></sec:authentication>
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>Welcome to the world of files!!!</h3>

                <div>
                    <div>
                        <form action="/search" method="GET">
                            <div class="row">
                                <div id="form-group-file" class="form-group col-lg-4">
                                    <label>Search: </label>
                                    <input type="text" name="searchText" class="form-inline">
                                    <input type="submit" value="Search" class="btn btn-default">
                                </div>
                            </div>
                        </form>
                    </div>
                    <c:choose>
                        <c:when test="${fn:length(fileMetadataList) eq 0}">
                            <span>No result!</span>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="file" items="${fileMetadataList}">
                                <p>
                                    <span>${file.user.firstName}</span> <span>: </span>
                                    <span>${file.description}</span>
                                    <span><a href="/download/${file.user.id}?name=${file.fileName}">Download</a></span>
                                </p>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>


                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>