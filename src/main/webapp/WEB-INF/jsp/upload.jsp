<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:layout>
    <jsp:attribute name="header"><spring:message code="rabbitholes.homepage.title"/></jsp:attribute>
    <jsp:body>
        <sec:authentication property="principal.id" var="user_id"></sec:authentication>
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>Share your file to the world!!!</h3>
                <div>
                    <c:forEach var="file" items="${fileMetadataList}">
                        <p>
                            <span>${file.user.firstName}</span> <span>: </span>
                            <span>${file.description}</span>
                            <span><a href="/download/${file.user.id}?name=${file.fileName}">Download</a></span>
                            <a href="${pageContext.request.contextPath}/delete?fileId=${file.id}&r=upload">Delete</a>
                        </p>
                    </c:forEach>
                </div>
                <form:form action="/upload" method="POST" role="form" commandName="file" enctype="multipart/form-data">
                    <input type="hidden"
                           name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                    <div class="row">
                        <div id="form-group-file" class="form-group col-lg-4">
                            <label class="control-label" for="file"><spring:message code="label.vault.file"/>:</label>
                            <input id="file" name="file" type="file" class="form-inline"/>
                        </div>
                    </div>

                    <div class="row">
                        <div id="form-group-desc" class="form-group col-lg-4">
                            <label class="control-label" for="desc"><spring:message code="label.vault.description"/>:</label>
                            <input id="desc" name="desc" type="text" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <button type="submit" class="btn btn-default"><spring:message code="label.vault.submit.button"/></button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </jsp:body>
</t:layout>